# Build brother_ql_net_emulator
FROM docker.io/rust:1.84-alpine3.20 AS net_emulator
RUN apk add --no-cache musl-dev

ADD . /build
WORKDIR /build

RUN --mount=type=cache,target=/build/target/ \
    --mount=type=cache,target=/usr/local/cargo/git/db \
    --mount=type=cache,target=/usr/local/cargo/registry/ \
    cargo build --release --locked && cp /build/target/release/brother_ql_net_emulator /build/

# Prepare runtime environment
FROM docker.io/alpine:3.20
RUN apk add --no-cache git python3 py3-pip py3-usb libusb-dev

RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
RUN pip install git+https://github.com/LunarEclipse363/brother_ql/@9bd8d45e1d2d6afc4bb0989d3486ff8c884c19c8

# Entrypoint script
COPY <<EOF /entrypoint.sh
#!/bin/ash
export LIBUSB_DEBUG=4
brother_ql_net_emulator -c /data/config.kdl
EOF
RUN chmod +x /entrypoint.sh

COPY --from=net_emulator /build/brother_ql_net_emulator /usr/local/bin/

VOLUME /data
ENTRYPOINT /entrypoint.sh
