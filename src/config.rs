use std::net::IpAddr;

pub const DEFAULT_CONFIG: &'static str = include_str!("../config.example.kdl");

/// The struct holding all of the avilable settings
#[derive(Debug, Clone, knuffel::Decode)]
pub struct Config {
    #[knuffel(child)]
    pub network: NetworkConfig,
    #[knuffel(child)]
    pub connected_printer: ConnectedPrinterConfig,
    #[knuffel(child)]
    pub emulated_printer: EmulatedPrinterConfig,
    #[knuffel(child)]
    pub limits: LimitsConfig,
}

#[derive(Debug, Clone, knuffel::Decode)]
pub struct NetworkConfig {
    /// List of addresses to bind to
    #[knuffel(children(name = "listen"), unwrap(argument, str))]
    pub listening_addresses: Vec<IpAddr>,
}

#[derive(Debug, Clone, knuffel::Decode)]
pub struct ConnectedPrinterConfig {
    // Printer id string for brother_ql. Example: `usb://0x04f9:0x2028`
    #[knuffel(child, unwrap(argument))]
    pub path: String,
    // Model id string for brother_ql. Example: `QL-570`
    #[knuffel(child, unwrap(argument))]
    pub model: String,
}

#[derive(Debug, Clone, knuffel::Decode)]
pub struct EmulatedPrinterConfig {
    // Series code to advertise on the network.
    // Only needed when the real printer has a different series code than the target printer.
    // Example: `0x34` (for QL-580N, QL-1060N)
    #[knuffel(child, unwrap(argument))]
    pub series_code: ::std::option::Option<u8>,
    // Model code to advertise on the network.
    // You should choose a networked model closest to the real printer.
    // Example: `0x33` (for QL-580N)
    #[knuffel(child, unwrap(argument))]
    pub model_code: u8,
    // Model id string from brother_ql. Example: `QL-580N`
    #[knuffel(child, unwrap(argument))]
    pub model_name: String,
}

#[derive(Debug, Clone, knuffel::Decode)]
pub struct LimitsConfig {
    // Maximum length of time for which to cache the last status received from the printer.
    // None means it will stay valid forever until another expiry condition is met.
    #[knuffel(child, unwrap(argument))]
    pub status_expiration_seconds: ::std::option::Option<u32>,
    // Maximum size of received raster in MB
    #[knuffel(child, unwrap(argument))]
    pub max_raster_size_mb: usize,
}

impl Default for Config {
    fn default() -> Self {
        knuffel::parse("config.example.kdl", DEFAULT_CONFIG).unwrap()
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn validate_default_config() {
        super::Config::default();
    }
}
