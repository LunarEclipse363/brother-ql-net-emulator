use std::borrow::Cow;
use std::sync::Arc;
use std::{collections::HashMap, net::IpAddr};

use once_cell::sync::Lazy;
use rasn::types::{ObjectIdentifier, OctetString};
use rasn_smi::v1::{ObjectSyntax, SimpleSyntax};
use rasn_snmp::v1::{GetResponse, Message, Pdu, Pdus, VarBind};
use tokio::{net::UdpSocket, sync::mpsc, task::yield_now};

use crate::{Config, StatusRequest};

/// This struct is just to make working with OIDs slightly less Painful
#[derive(Clone, Debug)]
#[allow(unused, reason = "unused fields reserved for future use")]
struct StaticVarBind {
    /// Short name of what this is
    pub name: String,
    /// Long description of what this is
    pub description: String,
    /// OID
    pub key: ObjectIdentifier,
    /// Value
    pub value: ObjectSyntax,
}

impl Into<VarBind> for StaticVarBind {
    fn into(self) -> VarBind {
        VarBind {
            name: self.key,
            value: self.value,
        }
    }
}

impl StaticVarBind {
    /// Returns a new [StaticVarBind]
    ///
    /// This always takes an [ObjectIdentifier] as the name and an [OctetString] as the value.
    ///
    /// Panics:
    /// When the OID would be invalid according to [ObjectIdentifier] rules
    /// (this function unwraps the result of that struct's `.new()` method)
    pub fn new_simple<K: Into<std::borrow::Cow<'static, [u32]>>>(
        name: &str,
        description: &str,
        key: K,
        value: Option<OctetString>,
    ) -> Self {
        Self {
            name: name.to_owned(),
            description: description.to_owned(),
            key: ObjectIdentifier::new(key).unwrap(),
            value: match value {
                None => ObjectSyntax::Simple(SimpleSyntax::Empty),
                Some(v) => ObjectSyntax::Simple(SimpleSyntax::String(v.into())),
            },
        }
    }
}

static STATIC_VARBINDS: Lazy<HashMap<ObjectIdentifier, StaticVarBind>> = Lazy::new(|| {
    let mut hashmap = HashMap::new();
    [
        StaticVarBind::new_simple(
            "SerialNumber",
            "The last 9 characters of the serial number",
            vec![1, 3, 6, 1, 4, 1, 2435, 2, 3, 9, 4, 2, 1, 5, 5, 1, 0],
            Some("FOXFOXFOX".into()),
        ),
        StaticVarBind::new_simple(
            "NodeName",
            "",
            vec![1, 3, 6, 1, 4, 1, 1240, 2, 3, 4, 1, 1, 0],
            Some("Emulated Printer :3".into()),
        ),
        // I made this up based on Brother's MAC ranges :3
        // https://udger.com/resources/mac-address-vendor-detail?name=brother_industries_ltd
        // TODO: this likely should be the raw bytes and not a string representation
        StaticVarBind::new_simple(
            "MacAddress",
            "",
            vec![1, 3, 6, 1, 2, 1, 2, 2, 1, 6, 1],
            Some("B422000B00B5".into()),
        ),
        StaticVarBind::new_simple(
            "Location",
            "",
            vec![1, 3, 6, 1, 2, 1, 1, 6, 0],
            Some("A Foxgirl's Homelab".into()),
        ),
        StaticVarBind::new_simple(
            "MibSysObjectId0",
            "unknown OID, requested but not used by the mobile app",
            vec![1, 3, 6, 1, 2, 1, 1, 2, 0],
            Some("uwu".into()),
        ),
        StaticVarBind::new_simple(
            "SmiEnterprises",
            "unknown OID, requested but not used by the mobile app",
            vec![1, 3, 6, 1, 4, 1, 11, 2, 3, 9, 1, 1, 3, 0],
            Some("owo".into()),
        ),
    ]
    .into_iter()
    .map(|vb| (vb.key.clone(), vb))
    .for_each(|(k, v)| {
        hashmap.insert(k, v);
    });
    return hashmap;
});

/// This will create a response to an SNMP request.
///
/// Failure will either result in an appropriate response
/// or returning None, in which case just don't respond to the request
pub async fn respond_to_snmp_request(
    cfg: Arc<Config>,
    bytes: &[u8],
    status_request_sender: mpsc::Sender<crate::StatusRequest>,
) -> Option<Vec<u8>> {
    let message = match rasn::ber::decode::<Message<Pdus>>(bytes) {
        Ok(m) => m,
        Err(e) => {
            log::debug!("Failed to decode SNMP message: {}", e);
            return None;
        }
    };
    if message.version != rasn::types::Integer::from(0) || message.community != "public" {
        log::debug!(
            "Invalid SNMP version or community: version {}, community {:?}",
            message.version,
            message.community
        );
        return None;
    }
    let request = match message.data {
        Pdus::GetRequest(request) => request.0,
        p => {
            log::debug!("Unrecognized PDU: {:?}", p);
            return None;
        }
    };
    // I'm not checking the other parts because whatever ig?
    if request.error_status != rasn::types::Integer::from(0) {
        log::debug!(
            "SNMP request has a non-zero error status: {}",
            request.error_status
        );
        return None;
    }

    let mut response_varbinds = Vec::new();

    for request_varbind in request.variable_bindings {
        // OIDs that get special treatment (not static)
        let status_request_oid = ObjectIdentifier::new_unchecked(Cow::Owned(vec![
            1, 3, 6, 1, 4, 1, 2435, 3, 3, 9, 1, 6, 1, 0,
        ]));
        let model_name_oid =
            ObjectIdentifier::new_unchecked(Cow::Owned(vec![1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 3, 1]));

        if let Some(response_varbind) = STATIC_VARBINDS.get(&request_varbind.name) {
            response_varbinds.push(response_varbind.clone());
        } else if request_varbind.name == status_request_oid {
            // Status Request
            // printer status response, just like in the raster, 32 bytes
            let (channel, status_receiver) = tokio::sync::oneshot::channel();
            status_request_sender
                .send(StatusRequest { channel })
                .await
                .unwrap();
            match status_receiver.await.expect("sender disconnected") {
                Ok(status) => response_varbinds.push(StaticVarBind::new_simple(
                    "Status Request",
                    "printer status response, just like in the raster, 32 bytes",
                    status_request_oid.to_vec(),
                    Some(OctetString::from(Vec::from(patch_status(
                        cfg.clone(),
                        status.raw,
                    )))),
                )),
                Err(e) => {
                    log::warn!("Error while receiving status: {:?}", e);
                    return rasn::ber::encode(&Message {
                        version: 0.into(),
                        community: "public".into(),
                        data: Pdus::GetResponse(GetResponse(Pdu {
                            request_id: request.request_id,
                            error_status: Pdu::ERROR_STATUS_GEN_ERR.into(),
                            // I have no idea if this is the correct way to do it
                            error_index: request.error_index,
                            variable_bindings: vec![VarBind {
                                name: request_varbind.name,
                                value: ObjectSyntax::Simple(SimpleSyntax::Empty),
                            }],
                        })),
                    })
                    .ok();
                }
            }
        } else if request_varbind.name == model_name_oid {
            // This is a best guess, should hopefully work
            response_varbinds.push(StaticVarBind::new_simple(
                "Model",
                "The model of the label printer (ex. 'Brother QL-580N')",
                vec![1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 3, 1],
                Some(format!("Brother {}", cfg.emulated_printer.model_name).into()),
            ));
        } else {
            log::warn!(
                "Requested OID not recognized: {:?}",
                request_varbind.name.as_ref()
            );
            return rasn::ber::encode(&Message {
                version: 0.into(),
                community: "public".into(),
                data: Pdus::GetResponse(GetResponse(Pdu {
                    request_id: request.request_id,
                    error_status: Pdu::ERROR_STATUS_NO_SUCH_NAME.into(),
                    // I have no idea if this is the correct way to do it
                    error_index: request.error_index,
                    variable_bindings: vec![VarBind {
                        name: request_varbind.name,
                        value: ObjectSyntax::Simple(SimpleSyntax::Empty),
                    }],
                })),
            })
            .ok();
        }
    }

    return rasn::ber::encode(&Message {
        version: 0.into(),
        community: "public".into(),
        data: Pdus::GetResponse(GetResponse(Pdu {
            request_id: request.request_id,
            error_status: 0.into(),
            error_index: 0.into(),
            variable_bindings: response_varbinds.into_iter().map(|vb| vb.into()).collect(),
        })),
    })
    .ok();
}

pub async fn process_snmp(
    cfg: Arc<Config>,
    addr: IpAddr,
    status_request_sender: mpsc::Sender<crate::StatusRequest>,
) {
    let snmp_socket = Arc::new(UdpSocket::bind((addr, 161u16)).await.expect(
        "Failed to bind the SNMP socket. (Does the program have permissions to bind to post 161?)",
    ));

    loop {
        let mut snmp_buf = Vec::with_capacity(1024);
        let (len, addr) = match snmp_socket.recv_buf_from(&mut snmp_buf).await {
            Ok(x) => x,
            Err(e) => {
                log::warn!("Error when trying to receive bytes: {}", e);
                snmp_buf.clear();
                yield_now().await;
                continue;
            }
        };
        log::trace!("Received {} bytes on the SNMP port from {}", len, addr);

        let socket = snmp_socket.clone();
        let sender = status_request_sender.clone();
        let cfg = cfg.clone();
        tokio::spawn(async move {
            if let Some(bytes) = respond_to_snmp_request(cfg, &snmp_buf, sender).await {
                // This can be used for an amplification attack. Firewall your LAN.
                match socket.send_to(&bytes, addr).await {
                    Ok(_) => {
                        log::trace!("Responded to SNMP request from {} with {} bytes", addr, len)
                    }
                    Err(e) => log::warn!("Error when trying to send bytes: {}", e),
                };
            } else {
                log::warn!("Ignored unrecognized snmp request from {}", addr);
            }
        });
    }
}

fn patch_status(cfg: Arc<Config>, mut input: [u8; 32]) -> [u8; 32] {
    if let Some(series_code) = cfg.emulated_printer.series_code {
        input[3] = series_code;
    }
    input[4] = cfg.emulated_printer.model_code;
    input
}
