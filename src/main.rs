pub mod config;
pub mod snmp;

use clap::{ArgAction, Parser};
use config::Config;

use std::io::Write;
use std::net::IpAddr;
use std::path::PathBuf;
use std::process::Stdio;
use std::sync::Arc;

use chrono::{DateTime, Utc};
use eyre::{eyre, Result};
use tempfile::NamedTempFile;
use tokio::io::AsyncWriteExt;
use tokio::net::{TcpListener, TcpStream};
use tokio::process::Command;
use tokio::sync::mpsc;
use tokio::task::JoinSet;
use ulid::Ulid;

#[derive(clap::Parser)]
#[command(version, about)]
pub struct Cli {
    /// Config file location
    #[arg(short, long, value_name = "FILE")]
    config: Option<PathBuf>,
    /// Print default config to stdout and exit
    #[arg(long, action = ArgAction::SetTrue)]
    print_default_config: bool,
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init_timed();

    let args = Cli::parse();
    if args.print_default_config {
        println!("{}", config::DEFAULT_CONFIG);
        return;
    }

    let config: Arc<Config> = Arc::new({
        let path = args
            .config
            .as_deref()
            .expect("Please provide a config file.");
        let file = std::fs::read_to_string(path).expect("Error reading config file!");

        knuffel::parse(&path.as_os_str().to_string_lossy(), &file).unwrap()
    });
    log::info!(
        "Found valid config file at: {}",
        args.config.as_deref().unwrap().display()
    );

    let mut handles = JoinSet::new();

    let (status_request_sender, status_request_receiver) = mpsc::channel(512);
    let (print_request_sender, print_request_receiver) = mpsc::channel(8);

    for addr in &config.network.listening_addresses {
        handles.spawn(snmp::process_snmp(
            config.clone(),
            addr.clone(),
            status_request_sender.clone(),
        ));
        log::debug!("Spawned SNMP handle for address {}", addr.to_string());
        handles.spawn(raw_handler(
            config.clone(),
            addr.clone(),
            print_request_sender.clone(),
        ));
        log::debug!("Spawned Raw TCP handle for address {}", addr.to_string());
    }

    handles.spawn(printer_handler(
        config.clone(),
        status_request_receiver,
        print_request_receiver,
    ));
    log::debug!("Spawned Printer handle");

    while let Some(result) = handles.join_next().await {
        if let Err(e) = result {
            log::error!("Error in task: {}", e);
        }
    }
}

pub struct StatusRequest {
    channel: tokio::sync::oneshot::Sender<Result<StatusResponse>>,
}

#[derive(Clone, Copy, Debug)]
pub struct StatusResponse {
    raw: [u8; 32],
}

pub struct PrintRequest {
    job_id: Ulid,
    data: Vec<u8>,
    channel: Option<tokio::sync::oneshot::Sender<Result<PrintResponse>>>,
}

pub struct PrintResponse {
    // TODO - this is future-proofing for an API
}

async fn printer_handler(
    cfg: Arc<Config>,
    mut status_request_receiver: mpsc::Receiver<StatusRequest>,
    mut print_request_receiver: mpsc::Receiver<PrintRequest>,
) {
    let status_timeout = cfg
        .limits
        .status_expiration_seconds
        .map(|t| chrono::TimeDelta::seconds(t.into()));
    let mut status: Option<[u8; 32]> = None;
    let mut last_status_update: DateTime<Utc> = Utc::now();
    loop {
        let mut status_request = None;
        let mut print_request = None;
        tokio::select! {
            req = print_request_receiver.recv() => print_request = req,
            req = status_request_receiver.recv() => status_request = req,
        };

        if status_timeout
            .map(|t| Utc::now() - &last_status_update > t)
            .unwrap_or(false)
        {
            status = None;
        }
        while status.is_none() {
            status = match get_raw_status(cfg.clone()).await {
                Ok(status) => {
                    last_status_update = Utc::now();
                    Some(status)
                }
                Err(error) => {
                    log::error!("Error while trying to get status: {}", error);
                    None
                }
            };
            if status.is_none() {
                // Just keep retrying until we can get a status
                log::info!("Retrying status request in 5 seconds...");
                tokio::time::sleep(std::time::Duration::from_secs(5)).await;
            }
        }

        if let Some(status_request) = status_request {
            status_request
                .channel
                .send(Ok(StatusResponse {
                    raw: status.unwrap(),
                }))
                .expect("The receiver should never be dropped first.");
        }

        if let Some(print_request) = print_request {
            status = None;
            let mut command = Command::new("brother_ql")
                .env("BROTHER_QL_PRINTER", &cfg.connected_printer.path)
                .env("BROTHER_QL_MODEL", &cfg.connected_printer.model)
                .args(&["transpile", &cfg.emulated_printer.model_name, "-", "-"])
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
                .expect("Failed to spawn brother_ql");
            command
                .stdin
                .take()
                .unwrap()
                .write_all(print_request.data.as_ref())
                .await
                .expect("Failed to write stdin");
            let command = command.wait_with_output().await.unwrap();

            let transpiled_data;

            if command.status.success() {
                log::info!(
                    "Transpile command for job {} succeeded",
                    print_request.job_id
                );
                log::debug!("Stderr:\n{}", String::from_utf8_lossy(&command.stderr));

                transpiled_data = command.stdout;
            } else {
                log::warn!(
                    "Transpile command for job {} failed, status: {:?}",
                    print_request.job_id,
                    command.status.code()
                );
                log::warn!("Stderr:\n{}", String::from_utf8_lossy(&command.stderr));
                continue;
            }

            let mut tmp = NamedTempFile::with_prefix(format!(
                "brother_net_emulator_job-{}-",
                print_request.job_id
            ))
            .expect("Failed to create a temporary file.");

            tmp.write_all(transpiled_data.as_ref())
                .expect("Failed to write instruction file");

            let tmp = tmp.into_temp_path();

            let command = Command::new("brother_ql")
                .env("BROTHER_QL_PRINTER", &cfg.connected_printer.path)
                .env("BROTHER_QL_MODEL", &cfg.connected_printer.model)
                .args(&[
                    "send",
                    tmp.to_str().expect("Temp path is not valid unicode"),
                ])
                .output()
                .await
                .unwrap();

            if command.status.success() {
                log::info!("Print command for job {} succeeded", print_request.job_id);
                log::debug!("Stderr:\n{}", String::from_utf8_lossy(&command.stderr));
            } else {
                log::warn!(
                    "Print command for job {} failed, status: {:?}",
                    print_request.job_id,
                    command.status.code()
                );
                log::warn!("Stderr:\n{}", String::from_utf8_lossy(&command.stderr));
            }

            if let Some(_c) = print_request.channel {
                // TODO: send a response
            }

            if log::log_enabled!(log::Level::Debug) {
                match tmp.keep() {
                    Err(e) => log::warn!("Failed to persist temporary file: {}", e),
                    Ok(p) => log::debug!(
                        "Persisted instructions file for job: {}",
                        p.to_string_lossy()
                    ),
                }
            }
        };
    }
}

async fn get_raw_status(cfg: Arc<Config>) -> Result<[u8; 32]> {
    let command = Command::new("brother_ql")
        .env("BROTHER_QL_PRINTER", &cfg.connected_printer.path)
        .env("BROTHER_QL_MODEL", &cfg.connected_printer.model)
        .args(&["status", "-f", "raw_bytes"])
        .output()
        .await
        .unwrap();
    if command.status.success() {
        log::info!("Status request command succeeded:",);
        log::debug!(
            "Raw status (len: {}): {:X?}",
            command.stdout.len(),
            command.stdout
        );
        Ok(command.stdout.as_slice().try_into()?)
    } else {
        log::error!(
            "Status request command failed, status: {:?}",
            command.status.code()
        );
        log::error!("Stderr:\n{}", String::from_utf8_lossy(&command.stderr));
        return Err(eyre!("Command failed, status: {:?}", command.status.code()));
    }
}

async fn raw_handler(
    cfg: Arc<Config>,
    listening_addr: IpAddr,
    print_request_sender: mpsc::Sender<PrintRequest>,
) {
    let listener = TcpListener::bind((listening_addr, 9100))
        .await
        .expect(&format!("Failed to bind to tcp {}:9100", listening_addr));

    loop {
        match listener.accept().await {
            Err(e) => log::warn!("Error accepting raw connection: {}", e),
            Ok((stream, _addr)) => match raw_receive_all(cfg.clone(), stream).await {
                Err(e) => log::warn!("Error receiving raw data: {}", e),
                Ok(data) => {
                    print_request_sender
                        .send(PrintRequest {
                            job_id: Ulid::new(),
                            data,
                            channel: None,
                        })
                        .await
                        .unwrap();
                }
            },
        }
    }
}

async fn raw_receive_all(cfg: Arc<Config>, stream: TcpStream) -> Result<Vec<u8>> {
    // Set maximum raster size - a one page raster can take up to 6mb
    // That's uncompressed 30cmx103mm - almost 2x wider and 3x longer than supported by most printers
    let mut buf = Vec::with_capacity(cfg.limits.max_raster_size_mb * 1024usize.pow(2));
    log::info!(
        "Receiving raw instructions from {}",
        stream
            .peer_addr()
            .map(|a| a.to_string())
            .unwrap_or(String::from("<error>"))
    );

    loop {
        stream.readable().await?;

        match stream.try_read_buf(&mut buf) {
            Ok(0) => break,
            Ok(_n) => {}
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => continue,
            Err(e) => return Err(e.into()),
        }
    }

    log::info!(
        "Raw instructions received from {}, length: {}",
        stream
            .peer_addr()
            .map(|a| a.to_string())
            .unwrap_or(String::from("<error>")),
        buf.len()
    );

    Ok(buf)
}
