# Brother QL Net Emulator
Have you ever thought to yourself "damn I bought a USB-only printer but now I want to use the official mobile app for printing over WiFi"

No? Well I did.

Here's a fully usable rust program that pretends to be a Brother QL-580N printer on the local network, accepts print jobs from the iPrint&Label mobile app, makes some alterations to the instructions (removing unsupported opcodes, decompression), and sends it to a QL-570 via USB.

It can also receive instructions from the `brother_ql` command-line tool.

Fundamentally, this program tries to pretend to be a real networked printer as best as it can.  
I do not own a real networked label printer, so all of the behaviours are inferred from reverse-engineering the android app and researching resources available on the internet.

## Requirements
Currently this program requires the `transpile` branch of `brother_ql_next` ([available here](https://github.com/LunarEclipse363/brother_ql_next/tree/transpile)). I recommend installing it in a virtualenv as it's not yet stable.

See [`shell.nix`](./shell.nix) for the specific commit on that repo which this version of `brother_ql_net_emulator` has been tested to work with.

Note: as of commit [`1d0f064`](https://github.com/LunarEclipse363/brother_ql_next/commit/1d0f0641869116f186b939b6ba04540685f247ae) `brother_ql_next/transpile` only allows turning instructions for a QL-580N to instructions for a QL-570.

For USB access, you have 3 options:
- add the user running the program to the `lp` group (recommended)
- run the program as `root` (not recommended)
- add a udev rule that relaxes the permissions on the device file
  The udev rule will depend on your specific printer's USB identifiers, for a QL-570 it is as follows:
  ```
  ENV{ID_VENDOR_ID}=="04f9", ENV{ID_MODEL_ID}=="2028", MODE="0666"
  ```

The program requires access to port 161 for SNMP. This is normally not allowed without root.  
One way to allow it is by using `setcap` on the binary:
```
setcap 'cap_net_bind_service=+ep' /path/to/brother_ql_net_emulator
```
Alternatively you can use the `net.ipv4.ip_unprivileged_port_start` kernel parameter on linux to relax the port restrictions.

## Usage
To use the program, run it with `brother_ql_net_emulator -c <CONFIG_PATH>`

The [default configuration file](./config.example.kdl) comes with an extensive documentation of the options.

You can always save the default config using: `brother_ql_net_emulator --print-default-config > config.kdl`

```
Usage: brother_ql_net_emulator [OPTIONS]

Options:
  -c, --config <FILE>         Config file location
      --print-default-config  Print default config to stdout and exit
  -h, --help                  Print help
  -V, --version               Print version
```

## Docker
There is a `Dockerfile` and an [example compose.yaml](/compose.example.yaml) file provided in the repo.

Accessing devices in docker can be a pain to put it lightly.
With docker-compose, if a device gets disconnected, the container will go down and never get restarted again regardless of the restart policy.

In any case, if you still wish to use a container, it should work.

## Limitations
Currently, you cannot configure the static SNMP responses, however the hardcoded values *shouldn't* pose you problems.  
If you want/need to change them, you will need to edit the `STATIC_VARBINDS` variable in `src/snmp.rs` and compile your own version

MRs for making them more configurable are welcome, preferably as name/value map in the config file (separate struct maybe?).

## License
This project is licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/introduction-eupl-licence).

Unless explicitly stated otherwise, you agree to release any contributions submitted for inclusion with this work under the `EUPL-1.2` license, without any additional terms or conditions.

